import { Platform, StyleSheet } from "react-native";
import { Colors, Metrics, Fonts } from "./Themes/";

const styles = StyleSheet.create({
  container: {
    width: Metrics.WIDTH,
    height: Metrics.HEIGHT,
    backgroundColor: "white"
  },

  imgContainer: {
    width: Metrics.WIDTH,
    height: Metrics.HEIGHT * 0.3
  },

  textTitle: {
    color: "white",
    fontSize: Fonts.moderateScale(16),
    alignSelf: "center",
    marginTop: -1
  },

  textTitleBlack: {
    color: "#333",
    fontSize: Fonts.moderateScale(16),
    alignSelf: "center",
    marginTop: 5,
    marginBottom: 5
  },

  backArrow: {
    width: 30,
    alignItems: "center"
  },

  textInput: {
    borderWidth: 0.5,
    borderColor: Colors.greys,
    borderRadius: 5,
    alignSelf: "center",
    height: Metrics.WIDTH * 0.12,
    width: Metrics.WIDTH * 0.85,
    fontFamily: Fonts.type.SFUIDisplayRegular,
    color: Colors.loginBlue,
    marginTop: Metrics.WIDTH * 0.05,
    backgroundColor: "transparent",
    paddingLeft: 15
  },

  buttonSignUp: {
    color: Colors.loginGreen,
    backgroundColor: 'green',
    borderRadius: 20,
    alignSelf: "center",
    alignItems: "center",
    width: Metrics.WIDTH * 0.9,
    marginTop: Metrics.HEIGHT * 0.00,
    height: Metrics.HEIGHT * 0.06,
    justifyContent: "center",
    shadowOffset: { width: 0, height: 3 },
    shadowColor: Colors.shadows,
    shadowOpacity: 1.0,
    shadowRadius: 5,
    elevation: 1
  },

  buttonSignUp1: {
    
    borderRadius: 20,
    alignSelf: "center",
    alignItems: "center",
    width: Metrics.WIDTH * 0.5,
    marginTop: Metrics.HEIGHT * 0.01,
    height: Metrics.HEIGHT * 0.06,
    justifyContent: "center",
    shadowOffset: { width: 0, height: 3 },
    shadowColor: Colors.shadows,
    shadowOpacity: 1.0,
    shadowRadius: 5,
    elevation: 1
  },
  buttonSignUp2: {
    color: 'white',
    backgroundColor: Colors.frost,
    borderRadius: 20,
    alignSelf: "center",
    alignItems: "center",
    width: Metrics.WIDTH * 0.7,
    marginTop: Metrics.HEIGHT * 0.01,
    height: Metrics.HEIGHT * 0.06,
    justifyContent: "center",
    shadowOffset: { width: 0, height: 3 },
    shadowColor: Colors.shadows,
    shadowOpacity: 1.0,
    shadowRadius: 5,
    elevation: 1
  },

  textWhite: {
    color: "white",
    fontSize: Fonts.moderateScale(14),
    fontFamily: Fonts.type.SFUIDisplaySemibold
  },

  textTermsCondition: {
    color: Colors.loginBlue,
    fontSize: Fonts.moderateScale(12),
    fontFamily: Fonts.type.SFUIDisplaySemibold
  },

  header: {
    backgroundColor: Metrics.PRIMARY_COLOR,
    height: 56,
    borderBottomWidth: 0,
    ...Platform.select({
      ios: {},
      android: {
        marginTop: 10
      }
    }),
    elevation: 0
  },

  left: {
    flex: 0.5,
    backgroundColor: "transparent"
  },

  body: {
    flex: 2,
    alignItems: "center",
    backgroundColor: "transparent"
  },

  right: {
    flex: 0.5
  },

  tandcView: {
    flexDirection: "row",
    width: Metrics.WIDTH,
    justifyContent: "center",
    marginTop: Metrics.HEIGHT * 0.02
  },
  ands: {
    color: Colors.darktext,
    fontSize: Fonts.moderateScale(12),
    fontFamily: Fonts.type.SFUIDisplayRegular
  }
});

export default styles;
