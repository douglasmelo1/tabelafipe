/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */


import React, {Component} from 'react';

import { AdMobBanner } from 'react-native-admob';
import { Container } from 'native-base';
import Home from "./Home" ;
import Dashboard from "./Dashboard";
import Opinions from "./Opinions";
import Cadastro from "./Cadastro";

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
console.disableYellowBox = true; 

const Stack = createStackNavigator();

export default class App extends Component<Props> {

  render() {
   return (
     <Container>
      <NavigationContainer>
        <Stack.Navigator screenOptions={{headerShown: false}}  initialRouteName="Home">
          <Stack.Screen name="Home"  header={false}     component={Home} />
          <Stack.Screen name="Dashboard"  component={Dashboard} /> 
          <Stack.Screen name="Opinions"   component={Opinions} />
          <Stack.Screen name="Cadastro"   component={Cadastro} />
        </Stack.Navigator>
      </NavigationContainer>
      <AdMobBanner
        adSize="fullBanner"
        adUnitID="ca-app-pub-9534097551670907/7541812784"
        testDevices={[AdMobBanner.simulatorId]}
        onAdFailedToLoad={error => console.error(error)}
      />
      </Container>
   );
  }
}
