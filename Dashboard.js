import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  BackHandler,
  StatusBar,
  Platform,
  ScrollView
} from "react-native";
import { AdMobInterstitial } from 'react-native-admob';
import { Right, Header, Left, Body, Button } from "native-base";
import Ionicons from "react-native-vector-icons/Ionicons";

import styles from "./stylesV";
import stylesh from "./stylesH";

import { Images, Metrics } from "./Themes/";
import LinearGradient from "react-native-linear-gradient";

import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Entypo from "react-native-vector-icons/Entypo";
import Swiper from "react-native-swiper";

export default class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      colorFalseSwitchIsOn: "",
      switchOn1: true,
      switchOn2: false
    };
  }

  componentDidMount = async () => {
      var _this = this;
      const { params } = _this.props.route; 
      
      this.setState({
        marca : params.marca,
        id_marca: params.id_marca, 
        id_modelo: params.id_modelo,  
        nome: params.nome, 
        preco:params.preco, 
        combustivel:params.combustivel, 
        ano: params.ano_modelo, 
        referencia: params.referencia, 
        opinioesSize:params.opinioesSize,
        opinioes:params.opinioes
      });
      
    this._fetchLuke();  
  }

  _fetchLuke = async () => {
    
  }

  
 getMovies () {
  AdMobInterstitial.setAdUnitID('ca-app-pub-9534097551670907/7019461153');
  AdMobInterstitial.setTestDevices([AdMobInterstitial.simulatorId]);
  AdMobInterstitial.requestAd().then(() => AdMobInterstitial.showAd());
}

  onPress1 = () => {
    this.setState({ switchOn1: !this.state.switchOn1 });
  };

  onPress2 = () => {
    this.setState({ switchOn2: !this.state.switchOn2 });
  };

  componentWillMount() {
    BackHandler.addEventListener("hardwareBackPress", this.backPressed);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.backPressed);
  }

  backPressed = () => {
    this.props.navigation.navigate("Home");
    return true;
  };

  render() {
    if (Platform.OS === "android") {
      StatusBar.setBackgroundColor("#4bc58c", true);
      StatusBar.setTranslucent(true);
    }
    return (
      <View style={styles.mainview}>
        <View style={styles.mainGradiyantView}>
          <LinearGradient
            locations={[0.1, 0.98]}
            colors={["#58ca90", "#9be3a2"]}
            start={{ x: 0, y: 1 }}
            end={{ x: 1, y: 1 }}
            style={styles.mainGradiyantView}
          >
            <Header androidStatusBarColor={"transparent"} style={styles.header}>
              <Left style={styles.left}>
                <TouchableOpacity
                  onPress={() => this.props.navigation.goBack()}
                >
                  <Ionicons
                    name="ios-arrow-back"
                    color="#ffffff"
                    size={30}
                    style={{ marginLeft: 5 }}
                  />
                </TouchableOpacity>
              </Left>

              <Body style={styles.body}>
                <Text style={styles.Dashboardtext}>Informações</Text>
              </Body>

              <Right style={styles.right}>
                
              </Right>
            </Header>
            <View style={styles.mainSwiperView}>
              <Swiper
                style={styles.wrapper}
                
               
              >
                <View style={styles.slide1}>
                  <Text style={styles.FORDEDGEText}>{this.state.marca}</Text>
                  <Image 
                  source={{ uri: 'https://todasautopecasloja.com.br/api/fipe/carros/'+this.state.id_marca+'.jpg'}} 
                  style={styles.CarBg} />
                  <View style={{ flexDirection: "row", alignSelf: "center" }}>
                    
                    <Text style={styles.AddressText}>
                    {this.state.nome}
                    </Text>
                  </View>
                </View>

              </Swiper>
            </View>
          
          <View style={{marginTop: -50}}>
              
              <View
                style={{
                  flexDirection: "row"
                }}
              >
                <View style={styles.FuelSec}>
                  <Image source={Images.fuel} style={styles.fuelIcon} />
                  <View style={{ marginLeft: Metrics.HEIGHT * 0.01 }}>
                    <Text style={styles.FuelText}>{this.state.combustivel}</Text> 
                  </View>
                </View>
                <View style={styles.verticalSeparator} />

                <View style={styles.FuelSec}>
                  <Image source={Images.mnavigation} style={styles.fuelIcon} />
                  <View style={{ marginLeft: Metrics.HEIGHT * 0.01 }}>
                    <Text style={styles.FuelText}>Ano</Text>
                    <Text style={styles.FuelCount}>{this.state.ano}</Text>
                  </View>
                </View>
                <View style={styles.verticalSeparator} />
              </View>
              <View style={{marginTop: 10}}>
              <TouchableOpacity
                style={stylesh.buttonSignUp}
                onPress={() => { this.props.navigation.navigate('Opinions',{
                  marca : this.state.marca,
                  id_marca: this.state.id_marca, 
                  id_modelo: this.state.id_modelo,  
                  nome: this.state.nome, 
                  preco:this.state.preco, 
                  combustivel:this.state.combustivel, 
                  ano: this.state.ano_modelo, 
                  referencia: this.state.referencia, 
                  opinioesSize:this.state.opinioesSize,
                  opinioes:this.state.opinioes
                }) }}
                >
                <Text style={stylesh.textWhite}>Ver Opiniões dos Donos</Text>
              </TouchableOpacity>                
              </View>
            </View>
          </LinearGradient>
        </View>
        <View style={styles.MainDestailSec}>
          <ScrollView>
            <TouchableOpacity
              style={{ flexDirection: "row", justifyContent: "space-between" }}
              onPress={() =>
                this.props.navigation.navigate("TemperatureControl")
              }
            >
              <View
                style={{
                  flexDirection: "row",
                  marginLeft: Metrics.HEIGHT * 0.03,
                  marginTop: Metrics.HEIGHT * 0.02
                }}
              >
                <Image
                  source={Images.avg}
                  style={styles.AirConditionrIcon}
                />

                <Text style={styles.AirConditionerText}>Valor da tabela</Text>
              </View>
              <View
                style={{
                  marginLeft: Metrics.HEIGHT * 0.03,
                  marginTop: Metrics.HEIGHT * 0.02
                }}
              >
                <Text style={styles.NumberText}>{this.state.preco}</Text>
              </View>
            </TouchableOpacity>

            <View
              style={{ flexDirection: "row", justifyContent: "space-between" }}
            >
              <View
                style={{
                  flexDirection: "row",
                  marginLeft: Metrics.HEIGHT * 0.03,
                  marginTop: Metrics.HEIGHT * 0.02
                }}
              >
                <Image
                  source={Images.Duration}
                  style={styles.AirConditionrIcon}
                />

                <Text style={styles.AirConditionerText}>
                  Referência
                </Text>
              </View>
              <View
                style={{
                  marginLeft: Metrics.HEIGHT * 0.03,
                  marginTop: Metrics.HEIGHT * 0.02
                }}
              >
                <Text style={styles.NumberText}>{this.state.referencia}</Text>
              </View>
            </View>
            {/* <View style={styles.HorizontalDivider} /> */}
            <View>  
              <Button onPress={()=>{ Linking.openURL('https://play.google.com/store/apps/details?id=com.tabelafipe')}}  
                  style={stylesh.buttonSignUp1} full info ><Text>Avalie o aplicativo</Text></Button>
              <Button onPress={()=>{ this.getMovies()}}  
                style={stylesh.buttonSignUp2} full  ><Text>Assista Video AD para Contribuir</Text></Button>         
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}
