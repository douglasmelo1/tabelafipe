import React, { Component } from "react";
import {
  StatusBar,
  Platform,
  View,
  Image,
  Keyboard,
  TouchableOpacity,
  ImageBackground,
  ScrollView,
  BackHandler,
  Alert,
  I18nManager,
	Linking,
  Picker
} from "react-native";

import { Form, H3, Content, Item, Icon, Header, Left, Input, Right, Card, 
  CardItem, Body, Text, Button, Container, Spinner, Label, Title } from 'native-base';
 
// Screen Styles
import styles from "./stylesH";
import { Images, Metrics } from "./Themes/";

export default class Home extends Component {

  constructor(props) {
    super(props)

    this.state = {
      loading: false,
      id_marca: '',
      id_modelo: '',
      opiniao: '',
      page: 0,
      opinioesSize: 0,
      id_ano: '',
      marcas: [],
      opinioes: {},
      anos: [],
      cnpj: [],
      modelos: [],
      pros:''
    }

  }

  componentWillMount() {
    var that = this;
    BackHandler.addEventListener("hardwareBackPress", function() {
     // that.props.navigation.navigate("SignUp");
      return true;
    });
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = async () => {
    this.setState({ status: false });
  }

  _keyboardDidHide = async () => {
    this.setState({ status: true });
  }


  componentDidMount() {
    this._fetchLuke();
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow); //Caso mostre teclado esconde footer
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
    // this.timer = setInterval(() => this.getMovies(), 60000)
  }
  
  _fetchLuke = async () => {
    this.setState({ loading: true });
    let res = await fetch('https://fipeapi.appspot.com/api/1/carros/marcas.json');
    let resJson = await res.json();
console.log(resJson)
    this.setState({ marcas: resJson, loading: false });
  }

  async returnModelos(id_marca) {

    if (id_marca) {
      this.setState({ loading: true });
      let res = await fetch('https://fipeapi.appspot.com/api/1/carros/veiculos/' + id_marca + '.json');
      let resJson = await res.json();

      this.setState({ modelos: resJson, id_marca: id_marca, loading: false });
    }
  }

  async returnAnos(id_modelo) {


    const { id_marca } = this.state;

    if (id_marca && id_modelo) {
      this.setState({ loading: true });
      let res = await fetch('https://fipeapi.appspot.com/api/1/carros/veiculo/' + id_marca + '/' + id_modelo + '.json');
      let resJson = await res.json();

      this.setState({ anos: resJson, id_marca: id_marca, id_modelo: id_modelo, loading: false });
    }

  }

  async returnValor() {

    const { id_marca } = this.state;
    const { id_modelo } = this.state;
    const { id_ano } = this.state;
    var _this = this;
 console.log(id_marca, id_modelo, id_ano)
    if (id_marca && id_modelo && id_ano) {
      _this.setState({ loading: true });
      // console.warn('http://fipeapi.appspot.com/api/1/carros/veiculo/' + id_marca + '/' + id_modelo + '/' + id_ano + '.json')
      let res = await fetch('https://fipeapi.appspot.com/api/1/carros/veiculo/' + id_marca + '/' + id_modelo + '/' + id_ano + '.json');
      let result = await res.json();

      res = await fetch('https://todasautopecasloja.com.br/api/fipe/jsonLer.php?id_marca='+ id_marca + '&id_modelo=' + id_modelo);
      resJson = await res.json();
console.log('res=====>',res);
      _this.setState({ loading: false, id_marca: '', id_modelo: '', id_ano: ''});

      _this.props.navigation.navigate("Dashboard",{
        marca: result.marca,
        id_marca: id_marca, 
        id_modelo:id_modelo, 
        ano:id_ano, 
        nome:result.name, 
        combustivel:result.combustivel, 
        preco:result.preco, 
        ano_modelo:result.ano_modelo, 
        referencia:result.referencia, 
        opinioesSize:resJson.length,
        opinioes:resJson
      });
 
    }else{
      alert('Todos os campos são obrigatórios');
      return false;
    }
  }


  render() {
    StatusBar.setBarStyle("light-content", true);

    if (Platform.OS === "android") {
      StatusBar.setBackgroundColor("transparent", true);
      StatusBar.setTranslucent(true);
    }

    const imageUri =
      "https://todasautopecasloja.com.br/api/fipe/mercedes.jpg";
    return (
      <Container style={styles.container}>
        <ImageBackground style={styles.imgContainer} 
        source={Images.mercedes_logo}
        >
        </ImageBackground>
        <Text style={styles.textTitleBlack}>FAÇA SUA BUSCA</Text>
        <ScrollView>
        <Form>
              <Item stackedLabel>
                <Label style={{ fontWeight: 'bold' }}>Marca do Veículo</Label>
                <Picker style={styles.picker}
                  style={{ width: 250 }}
                  selectedValue={this.state.id_marca}
                  onValueChange={(id_marca) => this.returnModelos(id_marca)}>

                  <Picker.Item value="" label="Escolha a marca" />
                  {this.state.marcas.map((l, i) => {
                    return <Picker.Item value={l.id} label={l.name} key={i} />
                  })}

                </Picker>
              </Item>
              {
                this.state.id_marca ?              
                <Item stackedLabel>
                  <Label style={{ fontWeight: 'bold', paddingTop: 20 }}>Modelo do Veículo</Label>
                  <Picker style={styles.picker}
                    style={{ width: 250 }}
                    selectedValue={this.state.id_modelo}
                    onValueChange={(id_modelo) => this.returnAnos(id_modelo)}>

                    <Picker.Item value="" label="Escolha o modelo" />
                    {this.state.modelos.map((l, i) => {
                      return <Picker.Item value={l.id} label={l.name} key={i} />
                    })}

                  </Picker>
                </Item> : null
              }

              {
                this.state.id_modelo ?   
                <Item stackedLabel>
                  <Label style={{ fontWeight: 'bold', paddingTop: 20 }}>Ano do Veículo</Label>
                  <Picker style={styles.picker}
                    style={{ width: 250 }}
                    selectedValue={this.state.id_ano}
                    onValueChange={(id_ano) => this.setState({ id_ano })}>

                    <Picker.Item value="" label="Escolha o ano" />
                    {this.state.anos.map((l, i) => {
                      return <Picker.Item value={l.id} label={l.name} key={i} />
                    })}

                  </Picker>
                </Item> : null
              }
              
            </Form>

            <View>
                {
                  this.state.loading ?
                    <Spinner color='blue' />
                    :
                    <TouchableOpacity
                      style={styles.buttonSignUp}
                      onPress={() => { this.returnValor() }}
                    >
                      <Text style={styles.textWhite}>Buscar</Text>
                    </TouchableOpacity>
                }
                </View>
          <View style={styles.tandcView}>
            <TouchableOpacity onPress={() => Linking.openURL('https://newcodeit.com.br/')}>
              <Text style={styles.textTermsCondition}>Desenvolvido por NewCode IT</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </Container>
    );
  }
}
