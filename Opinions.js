import React, { Component } from "react";
import {
  Platform,
  StatusBar,
  View,
  Text,
  TouchableOpacity,
  BackHandler,
  Alert,
  Linking,
  Image
} from "react-native";
import { Header, Left, Right, Body, Content } from "native-base";
import { Metrics, Images } from "./Themes";
import styles from "./stylesO";
import stylesh from "./stylesH";
import Ionicons from "react-native-vector-icons/Ionicons";
import LinearGradient from "react-native-linear-gradient";
import Moment from "moment";

var screenName = "";

export default class Opinions extends Component {
  constructor(props) {
    super(props);

    this.state = {
      opinioesSize:[],
      opinioes:[]
    };
  }

  componentDidMount = async () => {

      

    var _this = this;
    const { params } = _this.props.route; 
        console.log('params opinioesSize', params.opinioesSize)

      this.setState({
        marca : params.marca,
        id_marca: params.id_marca, 
        id_modelo: params.id_modelo,  
        nome: params.nome, 
        preco:params.preco, 
        combustivel:params.combustivel, 
        ano: params.ano_modelo, 
        referencia: params.referencia

      }) 

      if(params.opinioesSize > 0){
        this.setState({
          opinioesSize:params.opinioesSize,
          opinioes:params.opinioes
        });
      }
       
      

      
    // this._fetchLuke();  
  }

  componentWillMount() {
    BackHandler.addEventListener("hardwareBackPress", this.backPressed);

    const { navigation } = this.props; 
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.backPressed);
  }

  backPressed = () => {
    this.props.navigation.goBack();
    return true;
  };

  render() {
    StatusBar.setBarStyle("light-content", true);
    if (Platform.OS === "android") {
      StatusBar.setBackgroundColor("#48c48a", true);
      StatusBar.setTranslucent(true);
    }

    return (
      <View style={styles.mainView}>
        <LinearGradient
          locations={[0.1, 0.75]}
          colors={["#48c48a", "#a2e6a5"]}
          start={{ x: 0, y: 1 }}
          end={{ x: 1, y: 1 }}
        >
          <Header style={styles.HeaderBg} transparent>
            <Left style={styles.left}>
              {screenName == "SideMenu" ? (
                <TouchableOpacity
                  onPress={() => this.props.navigation.openDrawer()}
                >
                  <Image
                    source={Images.MenuIcon}
                    style={{ width: 25, height: 25, resizeMode: "contain" }}
                  />
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate("Dashboard")}
                >
                  <Ionicons
                    name="ios-arrow-back"
                    color="#ffffff"
                    size={30}
                    style={{ marginLeft: 5 }}
                  />
                </TouchableOpacity>
              )}
            </Left>
            <Body style={styles.body}>
              <Text style={styles.headertitle}>Opiniões</Text>
            </Body>
            <Right style={styles.right} />
          </Header>
        </LinearGradient>
        <View style={styles.MainReanderBg}>
          <Content>
          <View>  
              <TouchableOpacity
                style={stylesh.buttonSignUp}
                onPress={() => { this.props.navigation.navigate('Cadastro',{
                  marca : this.state.marca,
                  id_marca: this.state.id_marca, 
                  id_modelo: this.state.id_modelo,  
                  nome: this.state.nome, 
                  preco:this.state.preco, 
                  combustivel:this.state.combustivel, 
                  ano: this.state.ano_modelo, 
                  referencia: this.state.referencia, 
                  opinioesSize:this.state.opinioesSize,
                  opinioes:this.state.opinioes
                }) }}
                >
                  {this.state.opinioesSize > 0 ?
                      <Text style={stylesh.textWhite}>+ Dê sua opinião</Text>
                      :
                      <Text style={stylesh.textWhite}>Seja o primeiro a opinar</Text>
                    }

                
              </TouchableOpacity>                
            </View>
            <View style={styles.HorizontalDivider} />
            {this.state.opinioes && this.state.opinioes.map((item, index) => {
              return (
                <View key={index} style={{ backgroundColor: "#fff" }}>

                  <TouchableOpacity
                    style={styles.mainContentBg}
                   
                  >
                    <View
                      style={{
                        flexDirection: "row",
                        justifyContent: "space-between",
                        marginTop: Metrics.HEIGHT * 0.01
                      }}
                    >
                      <Text style={styles.NotificationCode}>
                        Dono: {item.nome}
                      </Text>
                      <Text style={styles.NotificationHours}>
                      {Moment(item.data).format("DD-MM-YY")}
                      </Text>
                    </View>
                    <Text style={styles.NotificationAddressP}>
                      Prós: {item.pros}
                    </Text>
                    <Text style={styles.NotificationAddressC}>
                      Contras: {item.contras}
                    </Text>
                    <Text style={styles.NotificationAddressD}>
                      Defeitos: {item.contras}
                    </Text>
                    <View
                      style={{
                        flexDirection: "row",
                        ...Platform.select({
                          ios: {
                            marginTop: Metrics.HEIGHT * 0.01
                          },
                          android: {}
                        })
                      }}
                    >
                     
                      <Text style={styles.NotificationAddress}>
                       Opinião Geral: {item.opiniao} 
                      </Text>
                    </View>
                  </TouchableOpacity>
                  <View style={styles.HorizontalDivider} />
                </View>
              );
            })}
          </Content>
        </View>
      </View>
    );
  }
}
