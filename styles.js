import { StyleSheet,Dimensions } from "react-native";

const {width} = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
      borderRadius: 4,
      borderWidth: 0.5,
      padding: 15,
      borderColor: '#d6d7da',
      backgroundColor: "#ffffff"
    },
    containerComecar: {
      flex: 1,
      justifyContent: 'center',
      padding: 20,
      backgroundColor: '#FFFFFF',
    },
    backgroundContainer: {
     position: 'absolute',
     top: 0,
     bottom: 0,
     left: 0,
     right: 0,
   },
    content: {
      backgroundColor: "#ffffff"
    },
    picker: {
      width: 200,
    },
    textarea: {
      width: 200,
    },
    defaultWidth: {
      width: width/2,
    },
    bodyIcon: {
      color: 'white'
    },
    containerLogado: {
      flex: 1,
      justifyContent: 'center',
      backgroundColor: '#D7EFF1',
    },
    containerLogadoGreen: {
      flex: 1,
      justifyContent: 'center',
      backgroundColor: '#41941E',
    },
    buttonLinearText: {
      fontSize: 18,
      fontFamily: 'Gill Sans',
      textAlign: 'center',
      margin: 10,
      color: '#ffffff',
      backgroundColor: 'transparent',
    },
    contentLogado: {
      backgroundColor: '#FFFFFF',
      margin: 20,
      marginTop: 20,
      padding: 15,
      borderRadius: 10,
    },
    text: {
      textAlign: 'left',
      paddingBottom: 15
    },
    viewLogado: {
      justifyContent: 'center',
      color: '#FFFFFF',
    },
    logo: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#FFFFFF',
      paddingBottom: 25

    },
    button: {
      borderRadius: 10,
      margin: 10
    }
});

export default styles;
