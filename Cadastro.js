import React, { Component } from "react";
import {
  StatusBar,
  Platform,
  View,
  Image,
  Keyboard,
  TouchableOpacity,
  ImageBackground,
  ScrollView,
  Alert,
  BackHandler,
  I18nManager,
	Linking,
  Picker
} from "react-native";
import { Images, Metrics } from "./Themes/";
import axios from 'axios';

import { Form, H3, Content, Item, Icon, Header, Left, Input, Right, Card, 
  CardItem, Body, Text, Button, Container, Spinner, Label, Title } from 'native-base';

// Screen Styles
import styles from "./stylesH";
import { Colors } from "./Themes/";

export default class Cadastro extends Component {

  constructor(props) {
    super(props)

    this.state = {
      loading: false,
      id_marca: '',
      id_modelo: '',
      opiniao: '',
      page: 0,
      opinioesSize: 0,
      id_ano: '',
      marcas: [],
      opinioes: {},
      anos: [],
      cnpj: [],
      modelos: [],
      pros:''
    }

  }

  componentWillMount() {
    var that = this;
    BackHandler.addEventListener("hardwareBackPress", function() {
     // that.props.navigation.navigate("SignUp");
      return true;
    });
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = async () => {
    this.setState({ status: false });
  }

  _keyboardDidHide = async () => {
    this.setState({ status: true });
  }


  componentDidMount() {

    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow); //Caso mostre teclado esconde footer
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);

    var _this = this;
    const { params } = _this.props.route; 
    
    this.setState({ 
      id_marca: params.id_marca, 
      id_modelo: params.id_modelo
      
    });
  }
      

  addAdv = () => {
      
    const { nome } = this.state;
    const { contras } = this.state;
    const { pros } = this.state;
    const { defeitos } = this.state;
    const { opiniao } = this.state;

    if (!nome || !contras || !pros || !defeitos || !opiniao) {
      alert("Todos os campos são obrigatórios");
      return false;
    }

    var _this = this;

    _this.setState({ loading: true });


    axios.post('https://todasautopecasloja.com.br/api/fipe/jsonEscrever.php', {
        id_marca: _this.state.id_marca, 
        id_modelo:_this.state.id_modelo, 
        nome:nome,
        contras:contras,
        pros:pros,
        opiniao:opiniao,
        defeitos:defeitos
    })
    .then(function (response) {
      alert('Enviado com sucesso. Obrigado!');
      _this.setState({ page: 0, loading: false, id_marca: "", id_modelo: "", id_ano: ""  });
      _this.props.navigation.navigate("Home");
    })
    .catch(function (error) {
      console.log(error);
    });

  }

  render() {
   
    return (
      <Container style={styles.container}>  

        <Header androidStatusBarColor={"transparent"} style={styles.header}>
              <Left style={styles.left}>
                <TouchableOpacity
                  onPress={() => this.props.navigation.goBack()}
                >
                  <Image
                    source={Images.paginationLeftArrow}
                    style={{
                      height: Metrics.HEIGHT * 0.07,
                      width: Metrics.WIDTH * 0.07,
                      resizeMode: "contain"
                    }}
                  />
                </TouchableOpacity>
              </Left>

              <Body style={styles.body}>
                <Text style={styles.Dashboardtext}>Cadastre sua opinião</Text>
              </Body>

              <Right style={styles.right}>
                
              </Right>
            </Header>
 
        <ScrollView>
        <Form>
            <Item stackedLabel>
                <Label style={{ fontWeight: 'bold', paddingTop: 20 }}>Seu Nome</Label>
                <Input
                  placeholder="Insira seu nome"
                  value={this.state.nome}
                  autoCapitalize="none"
                  maxLength={50}
                  onChangeText={nome => {
                    this.setState({ nome: nome });
                  }}
                />
              </Item>

              <Item stackedLabel>
                <Label style={{ fontWeight: 'bold', paddingTop: 20 }}>Prós</Label>
                <Input
                  placeholder="Descreva pontos fortes do veículo"
                  value={this.state.pros}
                  autoCapitalize="none"
                  maxLength={250}
                  onChangeText={pros => {
                    this.setState({ pros: pros });
                  }}
                />
              </Item>

              <Item stackedLabel>
                <Label style={{ fontWeight: 'bold', paddingTop: 20 }}>Contra</Label>
                <Input
                  placeholder="Descreva pontos fracos do veículo"
                  value={this.state.contras}
                  autoCapitalize="none"
                  maxLength={250}
                  onChangeText={contras => {
                    this.setState({ contras: contras });
                  }}
                />
              </Item>
              
              <Item stackedLabel>
                <Label style={{ fontWeight: 'bold', paddingTop: 20 }}>Defeitos Apresentados</Label>
                <Input
                  placeholder="Descreva defeitos apresentados"
                  value={this.state.defeitos}
                  autoCapitalize="none"
                  maxLength={250}
                  numberOfLines={2}
                  onChangeText={defeitos => {
                    this.setState({ defeitos: defeitos });
                  }}
                />
              </Item>

              <Item stackedLabel>
                <Label style={{ fontWeight: 'bold', paddingTop: 20 }}>Opinião Geral</Label>
                <Input
                  placeholder="Fale sobre sua opinião final"
                  value={this.state.opiniao}
                  autoCapitalize="none"
                  maxLength={250}
                  onChangeText={opiniao => {
                    this.setState({ opiniao: opiniao });
                  }}
                />
              </Item>

              <View style={{ height: 20 }} />
              <View>
                {
                  this.state.loading ?
                    <Spinner color='blue' />
                    :
                    <Button success rounded block onPress={() => { this.addAdv() }} >
                      <Text> ENVIAR </Text></Button>
                }

              </View>
            </Form> 
        </ScrollView>
      </Container>
    );
  }
}
