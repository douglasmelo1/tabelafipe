/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.tabelafipe;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.tabelafipe";
  public static final String BUILD_TYPE = "release";
  public static final int VERSION_CODE = 6;
  public static final String VERSION_NAME = "6.0";
}
